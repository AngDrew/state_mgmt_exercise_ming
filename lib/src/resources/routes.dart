import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:st_mgmt/src/screens/counter/counter_v.dart';
import 'package:st_mgmt/src/screens/counter/counter_vm.dart';
import 'package:st_mgmt/src/screens/dashboard/dashboard_v.dart';
import 'package:st_mgmt/src/screens/todo/todo_v.dart';
import 'package:st_mgmt/src/screens/todo/todo_vm.dart';

const String rootRoute = '/';
const String counterRoute = '/counter';
const String shoppingCartRoute = '/shopping_cart';
const String todoRoute = '/todo';
const String dummyJsonRoute = '/dummy_json';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case rootRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) => DashboardScreen(),
      );
    case counterRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) =>
            ChangeNotifierProvider<CounterViewModel>(
          create: (BuildContext context) => CounterViewModel(),
          builder: (_, __) => CounterScreen(),
        ),
      );
    case todoRoute:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) =>
            ChangeNotifierProvider<TodoViewModel>(
          create: (BuildContext context) => TodoViewModel(),
          builder: (_, __) => TodoScreen(),
        ),
      );
    default:
      return MaterialPageRoute<dynamic>(
        settings: settings,
        builder: (BuildContext context) => Scaffold(
          body: Center(
            child: Text('404 Not Found'),
          ),
        ),
      );
  }
}
