import 'package:flutter/material.dart';
import 'package:st_mgmt/src/resources/routes.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'State Mgmt Exercises',
        ),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        children: <Widget>[
          MyCustomListTile(
            onPressed: () {
              Navigator.pushNamed(context, counterRoute);
            },
            title: 'Counter',
          ),
          MyCustomListTile(
            onPressed: () {
              Navigator.pushNamed(context, todoRoute);
            },
            title: 'To-Do',
          ),
          MyCustomListTile(
            onPressed: () {},
            title: 'Shopping Cart',
          ),
          MyCustomListTile(
            onPressed: () {},
            title: 'Json Dummy',
          ),
        ],
      ),
    );
  }
}

class MyCustomListTile extends StatelessWidget {
  const MyCustomListTile({
    Key key,
    @required this.onPressed,
    @required this.title,
  }) : super(key: key);

  final void Function() onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ElevatedButton(
        onPressed: onPressed,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
          ),
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(title),
              const Icon(Icons.keyboard_arrow_right_rounded),
            ],
          ),
        ),
      ),
    );
  }
}
