import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:st_mgmt/src/screens/todo/checklist_todo_m.dart';
import 'package:st_mgmt/src/screens/todo/todo_m.dart';
import 'package:st_mgmt/src/screens/todo/todo_vm.dart';

class TodoScreen extends StatefulWidget {
  const TodoScreen({Key key}) : super(key: key);

  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('ToDo'),
      ),
      body: Consumer<TodoViewModel>(
        builder: (BuildContext context, TodoViewModel value, Widget child) =>
            ListView(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          children: <Widget>[
            for (final TodoModel todo in value.todos)
              NotesTile(
                todo,
                () {},
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (_) => Container(
              // width: size.width,
              height: size.width / 2,
              // color: Colors.blue,
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 16),
                  SizedBox(
                    width: size.width - 32,
                    height: 48,
                    child: ElevatedButton(
                      onPressed: () {
                        // go to Create Todo with Checklist
                        Navigator.pop(context);
                      },
                      child: Text('Todo With Checklist'),
                    ),
                  ),
                  const SizedBox(height: 16),
                  SizedBox(
                    width: size.width - 32,
                    height: 48,
                    child: ElevatedButton(
                      onPressed: () {
                        // go to Create Todo without Checklist
                        Navigator.pop(context);
                      },
                      child: Text('Todo Only'),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class NotesTile extends StatelessWidget {
  const NotesTile(
    this.todoModel,
    this.onTap, {
    Key key,
  }) : super(key: key);

  final TodoModel todoModel;
  final void Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey[200],
          borderRadius: BorderRadius.circular(8),
        ),
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.symmetric(vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(todoModel.title),
            Text(todoModel.description),
            if (todoModel.checkList != null)
              for (ChecklistTodoModel cl in todoModel.checkList)
                CheckboxListTile(
                  value: cl.isChecked,
                  title: Text(cl.task),
                  onChanged: (bool value) {
                    context.read<TodoViewModel>().changeCheckedStatus(
                          value,
                          todoModel,
                          todoModel.checkList.indexOf(cl),
                        );
                  },
                ),
          ],
        ),
      ),
    );
  }
}
