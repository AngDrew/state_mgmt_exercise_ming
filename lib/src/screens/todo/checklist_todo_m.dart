class ChecklistTodoModel {
  const ChecklistTodoModel(
    this.task,
    this.isChecked,
  );

  final String task;
  final bool isChecked;

  ChecklistTodoModel copyWith({
    String task,
    bool isChecked,
  }) {
    return ChecklistTodoModel(task ?? this.task, isChecked ?? this.isChecked);
  }
}
