import 'package:st_mgmt/src/screens/todo/checklist_todo_m.dart';

class TodoModel {
  const TodoModel(this.title, this.description, {this.checkList});

  factory TodoModel.fromJson(Map<String, dynamic> map) {
    if (map == null) {
      return null;
    }

    return TodoModel(
      map['title'],
      map['description'],
      checkList: map['check_list'],
    );
  }

  TodoModel copyWith({
    String title,
    String description,
    List<ChecklistTodoModel> checkList,
  }) {
    return TodoModel(
      title ?? this.title,
      description ?? this.description,
      checkList: checkList ?? this.checkList,
    );
  }

  final String title;
  final String description;
  final List<ChecklistTodoModel> checkList;
}
