import 'package:flutter/foundation.dart';
import 'package:st_mgmt/src/screens/todo/checklist_todo_m.dart';
import 'package:st_mgmt/src/screens/todo/todo_m.dart';

class TodoViewModel extends ChangeNotifier {
  List<TodoModel> todos = <TodoModel>[
    TodoModel('Title Random', 'Desc apa adanya'),
    TodoModel(
      'Title ehem',
      'Desc ehem',
      checkList: <ChecklistTodoModel>[
        ChecklistTodoModel('task 1', false),
        ChecklistTodoModel('task 2', true),
        ChecklistTodoModel('task 3', false),
        ChecklistTodoModel('task 4', true),
        ChecklistTodoModel('task 5', true),
      ],
    ),
    TodoModel('Title Mbo', 'Desc wkwkwk'),
  ];

  void editTodo({String title, String desc, List<TodoModel> todos}) {
    //
  }

  void changeCheckedStatus(
    bool newVal,
    TodoModel todoModel,
    int indexOfChecklist,
  ) {
    ChecklistTodoModel cl = todoModel.checkList[indexOfChecklist];
    cl = cl.copyWith(isChecked: newVal);
    todoModel.checkList[indexOfChecklist] = cl;

    notifyListeners();
  }
}
