import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:st_mgmt/src/screens/counter/counter_vm.dart';

class CounterScreen extends StatefulWidget {
  @override
  _CounterScreenState createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  @override
  Widget build(BuildContext context) {
    print('counter exercise is rebuilding');

    return Scaffold(
      appBar: AppBar(
        title: Text('Counter Exercise'),
      ),
      body: Center(
        child: Consumer<CounterViewModel>(
          builder: (_, CounterViewModel value, __) => Text(
            '${value.counterValue}',
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: context.read<CounterViewModel>().increment,
        // onPressed: context.watch<CounterViewModel>().increment,
        child: Icon(Icons.add),
      ),
    );
  }
}
