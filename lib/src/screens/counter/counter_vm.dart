import 'package:flutter/foundation.dart';

class CounterViewModel extends ChangeNotifier {
  int _counterValue = 0;
  int get counterValue => _counterValue;

  void increment() {
    _counterValue++;

    notifyListeners();
  }
}
