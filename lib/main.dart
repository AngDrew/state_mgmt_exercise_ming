import 'package:flutter/material.dart';
import 'package:st_mgmt/src/resources/routes.dart';
import 'package:st_mgmt/src/screens/dashboard/dashboard_v.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Mgmt',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: rootRoute,
      onGenerateRoute: generateRoute,
      home: DashboardScreen(),
    );
  }
}
